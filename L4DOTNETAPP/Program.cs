﻿using System;

namespace L4DOTNETAPP
{
    class Program
    {
        static int[] Kopiuj(int[] tablica)
        {
            int[] nowaTablica = new int[tablica.Length];

            for(int i = 0; i < tablica.Length; i++)
            {
                nowaTablica[i] = tablica[i];
            }

            return nowaTablica;
        }

        static void Main(string[] args)
        {
        


            int[] x = {1,2,3,4,5};
            int[] y = new int[x.Length];

            for(int i = 0; i < x.Length; i++)
            {
                y[i] = x[i];
            }

            x[2] = 1503;

            for (int i = 0; i < x.Length; i++)
            {
               // Console.Write(x[i] + " ");
            }

            Console.WriteLine();

            for (int i = 0; i < x.Length; i++)
            {
               // Console.Write(y[i] + " ");
            }


            int[,] x2 =
            {
                { 1, 2, 3, 4, 5 },
                { 1, 2, 3, 4, 5 }
            };

        
            for(int i = 0; i < x2.GetLength(0); i++)
            {
                for(int j = 0; j < x2.GetLength(1); j++)
                {
                 //   Console.Write(x2[i,j]);
                }
                Console.WriteLine();
            }

            int[][] x3 =
                {
                    new int[] { },
                    new[] { 1,2,3,4,5,6,7,8,9},
                    new[] { 1 },
                    new[] { 1, 2 },
                    new[] { 1,2,3},
                    new[] { 1,2,3,4,5,6,7},
                    new[] { 1,2,3},
                    new[] { 1, 2 },
                    new[] { 1, },
                    new[] { 1,2,3,4,5,6,7,8,9}
                };




            /*   for(int i = 0; i < x3.Length; i++)
               {
                   for (int j = 0; j < x3[i].Length; j++)
                       {
                       Console.Write(x3[i][j]);
                       }
                       Console.WriteLine();
               } */

            int[] jakasTablica = { 5, 6, 8, 3, 2, 6 };

            int[] jakasNowaTablica = Kopiuj(jakasTablica);

            for(int i = 0; i < jakasNowaTablica.Length; i++)
            {
                Console.WriteLine(jakasNowaTablica[i]);
            }







        }
    }
}
